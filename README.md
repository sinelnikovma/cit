Тестовый пример телефонного справочника на базе Spring для Центра информационных проектов.

###Результат сборки CodeShip###
* Автор: Sinelnikov Maksim
* Codeship : [![Codeship Status for sinelnikovma/cit](https://app.codeship.com/projects/41b04d40-00b4-0137-a5a8-02827f045340/status?branch=master)](/projects/324724)

####Требования для запуска:####
* Установлен сервер БД - PostgreSQL
* Установлен Node.js и пакетный менеджер npm

####Порядок запуска для среды разработки:####

1. Скопировать удалённый репозиторий на локальный компьютер командой **git clone https://bitbucket.org/sinelnikovma/cit**
2. В корневом каталоге локального репозитория набрать команду **npm install** для скачивания нужных модулей.
3. В терминале набрать команду **npm start** для запуска сервера разработки webpack
4. Запустить сервер Spring из среды разработки (класс App.java)
5. Телефонный справочник будет доступен в браузере по адресу http://localhost:8080/index.html 


Документация доступных методов в Swagger - http://localhost:8080/swagger-ui.html

Тест работоспособности Swagger - http://localhost:8080/v2/api-docs

Как установить Node.js и npm https://www.youtube.com/watch?v=ZNjnM0Fyn4E&t=0s&list=PLU2ftbIeotGog0MpNc0kyeWTulNpGhoAh&index=3

