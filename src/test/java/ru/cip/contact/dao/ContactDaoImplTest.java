package ru.cip.contact.dao;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import ru.cip.contact.model.Contact;
import ru.cip.user.model.User;

@RunWith(SpringRunner.class)
public class ContactDaoImplTest {

	private User user1;
	private User user2;
	private User user3;
	private User user4;
	private Contact contact1;
	private Contact contact2;
	private Contact contact3;
	private Contact contact4;
	private Set<Contact> contactSet;

	@MockBean
	private ContactDao contactDao;

	@Before
	public void setUp() throws Exception {

		user1 = new User("Вася", "Пупкин");
		user2 = new User("Петя", "Федорчук");
		user3 = new User("Коля", "Пупкин");
		user4 = new User("Ваня", "Иванов");
		contact1 = new Contact("VK", "VKontakte", user1);
		contact2 = new Contact("FB", "Facebook", user1);
		contact3 = new Contact("SK", "Skype", user2);
		contact4 = new Contact("VK", "VKontakte", user1);

		contactSet = new HashSet<>();
		contactSet.add(contact1);
		contactSet.add(contact2);
		contactSet.add(contact3);
		contactSet.add(contact4);
	}

	@Test
	public final void testGetAll() {

		Mockito.doReturn(contactSet).when(contactDao).getAll();

		final Set<Contact> set = contactDao.getAll();

		Assert.assertNotNull(set);
		Assert.assertEquals(set, contactSet);

	}

	@Test
	public final void testGetById() {

		Mockito.doReturn(contact1).when(contactDao).getById(1L);

		final Contact contact = contactDao.getById(1L);

		Assert.assertNotNull(contact);
		Assert.assertEquals(contact, contact1);
	}

	@Test
	public final void testGetByName() {
		Mockito.doReturn(contactSet).when(contactDao).getByName("con1");

		final Set<Contact> contact = contactDao.getByName("con1");

		Assert.assertNotNull(contact);
		Assert.assertEquals(contact, contactSet);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public final void testGetByNameFailure() {
		Mockito.doThrow(new DataIntegrityViolationException("contact id not found")).when(contactDao).getById(10L);

		final Contact contact = contactDao.getById(10L);

	}

}
