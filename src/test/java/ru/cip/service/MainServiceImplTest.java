package ru.cip.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import ru.cip.contact.dao.ContactDao;
import ru.cip.phone.dao.PhoneDao;
import ru.cip.user.dao.UserDao;
import ru.cip.view.DataUser;

@RunWith(SpringRunner.class)
public class MainServiceImplTest {

	private DataUser dataUser = new DataUser();
	private List<DataUser> dataUserList = new ArrayList<>();

	@MockBean
	private UserDao userDao;

	@MockBean
	private ContactDao contactDao;

	@MockBean
	private PhoneDao phoneDao;

	@MockBean
	private MainServiceImpl service;

	@Before
	public void setUp() throws Exception {
		dataUser.name = "Вася";
		dataUser.surname = "Пупкин";
		dataUser.typeContact = "VK";
		dataUser.nameContact = "VKontakte";
		dataUser.phone = "909-55-77-88";

		dataUserList.add(dataUser);
	}

	@Test
	public final void testGetAll() {
		Mockito.doReturn(dataUserList).when(service).getAll();

		final List<DataUser> list = service.getAll();

		Assert.assertNotNull(list);
		Assert.assertEquals(list, dataUserList);
	}

	@Test
	public final void testGetByFilter() {
		Mockito.doReturn(dataUserList).when(service).getByFilter(dataUser);

		final List<DataUser> list = service.getByFilter(dataUser);

		Assert.assertNotNull(list);
		Assert.assertEquals(list, dataUserList);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public final void testGetByFilterFailure() {

		Mockito.doThrow(new DataIntegrityViolationException("not found")).when(service).getByFilter(dataUser);

		final List<DataUser> list = service.getByFilter(dataUser);
	}

}
