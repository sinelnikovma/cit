package ru.cip.user.dao;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import ru.cip.user.model.User;

@RunWith(SpringRunner.class)
public class UserDaoImplTest {

	private User user1;
	private User user2;
	private User user3;
	private User user4;
	private Set<User> userSet = new HashSet<>();

	@MockBean
	private UserDao userDao;

	@Before
	public void setUp() throws Exception {
		user1 = new User("Вася", "Пупкин");
		user2 = new User("Петя", "Федорчук");
		user3 = new User("Коля", "Пупкин");
		user4 = new User("Ваня", "Иванов");
		userSet.add(user1);
		userSet.add(user2);
		userSet.add(user3);
		userSet.add(user4);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testGetAll() {
		Mockito.doReturn(userSet).when(userDao).getAll();

		final Set<User> list = userDao.getAll();

		Assert.assertNotNull(list);
		Assert.assertEquals(list, userSet);
	}

	@Test
	public final void testGetByFilter() {
		Mockito.doReturn(userSet).when(userDao).getByFilter(user1);

		final Set<User> list = userDao.getByFilter(user1);

		Assert.assertNotNull(list);
		Assert.assertEquals(list, userSet);
	}

	@Test
	public final void testGetById() {

		Mockito.doReturn(user1).when(userDao).getById(1L);

		final User user = userDao.getById(1L);

		Assert.assertNotNull(user);
		Assert.assertEquals(user, user1);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public final void testGetByIdFailure() {
		Mockito.doThrow(new DataIntegrityViolationException("user id not found")).when(userDao).getById(10L);

		final User user = userDao.getById(10L);
	}
}
