package ru.cip.phone.dao;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import ru.cip.phone.model.Phone;

@RunWith(SpringRunner.class)
public class PhoneDaoImplTest {

	private Phone phone1;
	private Phone phone2;
	private Phone phone3;
	private Phone phone4;
	private Set<Phone> phoneSet;

	@MockBean
	private PhoneDao phoneDao;

	@Before
	public void setUp() throws Exception {

		phone1 = new Phone("8(909)-555-11-99");
		phone2 = new Phone("8(909)-555-22-99");
		phone3 = new Phone("8(909)-555-33-99");
		phone4 = new Phone("8(909)-555-44-99");

		phoneSet = new HashSet<>();

		phoneSet.add(phone1);
		phoneSet.add(phone2);
		phoneSet.add(phone3);
		phoneSet.add(phone4);
	}

	@Test
	public final void testGetAll() {

		Mockito.doReturn(phoneSet).when(phoneDao).getAll();

		final Set<Phone> phones = phoneDao.getAll();

		Assert.assertNotNull(phones);
		Assert.assertEquals(phones, phoneSet);
	}

	@Test
	public final void testGetById() {

		Mockito.doReturn(phone2).when(phoneDao).getById(2L);

		final Phone phone = phoneDao.getById(2L);

		Assert.assertNotNull(phone);
		Assert.assertEquals(phone, phone2);
	}

	@Test
	public final void testGetByName() {

		Mockito.doReturn(phoneSet).when(phoneDao).getByName("8(909)-555-22-99");

		final Set<Phone> phone = phoneDao.getByName("8(909)-555-22-99");

		Assert.assertNotNull(phone);
		Assert.assertEquals(phone, phoneSet);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public final void testGetByNameFailure() {

		Mockito.doThrow(new DataIntegrityViolationException("contact id not found")).when(phoneDao).getByName("");

		phoneDao.getByName("");
	}

}
