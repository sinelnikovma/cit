package ru.cip.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ru.cip.service.MainServiceImpl;
import ru.cip.view.DataUser;

@RestController
@RequestMapping(value = "/api/phone")
public class MainController {

	private final MainServiceImpl service;

	@Autowired
	public MainController(MainServiceImpl service) {
		this.service = service;
	}

	@ApiOperation(value = "Ресурс для получения списка всего телефонного справочника", nickname = "showAll", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Not found"), @ApiResponse(code = 500, message = "Failure") })
	@GetMapping
	public List<DataUser> showAll() {
		return service.getAll();
	}

	@ApiOperation(value = "Ресурс для получения отфильтрованного списка телефонного справочника", nickname = "getByFilter", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Not found"), @ApiResponse(code = 500, message = "Failure") })
	@PostMapping
	public List<DataUser> getByFilter(@RequestBody DataUser body) {

		System.out.println(body);

		return service.getByFilter(body);

	}
}
