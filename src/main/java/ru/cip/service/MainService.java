package ru.cip.service;

import java.util.List;

import ru.cip.view.DataUser;

public interface MainService {
	public List<DataUser> getAll();

	public List<DataUser> getByFilter(DataUser filter);
}
