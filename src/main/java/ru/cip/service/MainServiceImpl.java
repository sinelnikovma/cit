package ru.cip.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ru.cip.contact.dao.ContactDao;
import ru.cip.contact.model.Contact;
import ru.cip.phone.dao.PhoneDao;
import ru.cip.phone.model.Phone;
import ru.cip.user.dao.UserDao;
import ru.cip.user.model.User;
import ru.cip.view.DataUser;

@Service
public class MainServiceImpl implements MainService {

	private final ContactDao contactDao;
	private final PhoneDao phoneDao;
	private final UserDao userDao;

	private final static MapperFacade mapperContact = facade(Contact.class, DataUser.class);
	private final static MapperFacade userFacade = facade(DataUser.class, User.class);
	private final static MapperFacade phoneFacade = facade(DataUser.class, Phone.class);
	private final static MapperFacade contactFacade = facade(DataUser.class, Contact.class);

	private static MapperFacade facade(Class src, Class dest) {
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		mapperFactory.classMap(src, dest).byDefault().register();
		MapperFacade mapperFacade = mapperFactory.getMapperFacade();

		return mapperFacade;
	}

	@Autowired
	public MainServiceImpl(UserDao userDao, ContactDao contactDao, PhoneDao phoneDao) {
		this.contactDao = contactDao;
		this.phoneDao = phoneDao;
		this.userDao = userDao;
	}

	@Override
	@Transactional(readOnly = true)
	public List<DataUser> getAll() {

		final Set<Contact> contactSet = contactDao.getAll();
		final List<DataUser> dataUser;

		dataUser = makeDataUser(contactSet);

		return dataUser;
	}

	@Override
	@Transactional
	public List<DataUser> getByFilter(DataUser filter) {

		List<DataUser> dataUser;

		User user = userFacade.map(filter, User.class);
		Contact contact = contactFacade.map(filter, Contact.class);
		Phone phone = phoneFacade.map(filter, Phone.class);

		final Set<User> userSet = userDao.getByFilter(user);
		final Set<Contact> contactSet = contactDao.getByName(contact.getNameContact());
		final Set<Phone> phoneSet = phoneDao.getByName(phone.getPhone());

		dataUser = makeDataUserByFilter(userSet, contactSet, phoneSet);

		return dataUser;
	}

	private List<DataUser> makeDataUser(Set<Contact> contactSet) {

		List<DataUser> dtList = new ArrayList<>();

		for (Contact contact : contactSet) {
			if (contact.getSetPhone().isEmpty()) {
				setUserIfPhoneEmpty(dtList, contact);
			}

			for (Phone phone : contact.getSetPhone()) {
				setUserWithPhone(dtList, contact, phone);

			}
		}

		return dtList;
	}

	private List<DataUser> makeDataUserByFilter(Set<User> userSet, Set<Contact> contactSet, Set<Phone> phoneSet) {

		List<DataUser> result = new ArrayList<>();

		for (Contact contact : contactSet) {
			if (userSet.contains(contact.getUser())) {

				if (contact.getSetPhone().isEmpty()) {
					setUserIfPhoneEmpty(result, contact);
				}
				for (Phone phone : contact.getSetPhone()) {
					if (phoneSet.contains(phone)) {
						setUserWithPhone(result, contact, phone);
					}
				}
			}
		}
		return result;
	}

	private void setUserIfPhoneEmpty(List<DataUser> dtList, Contact contact) {
		DataUser du;
		du = mapperContact.map(contact, DataUser.class);
		du.name = contact.getUser().getName();
		du.surname = contact.getUser().getSurname();
		dtList.add(du);
	}

	private void setUserWithPhone(List<DataUser> result, Contact contact, Phone phone) {
		DataUser dataUser;
		dataUser = mapperContact.map(contact, DataUser.class);
		dataUser.name = contact.getUser().getName();
		dataUser.surname = contact.getUser().getSurname();
		dataUser.phone = phone.getPhone();
		result.add(dataUser);
	}
}
