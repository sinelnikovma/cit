package ru.cip.utils;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@ControllerAdvice(basePackages = "ru.cip")
public class ResponseBodyAdviceImpl implements ResponseBodyAdvice<Object> {

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {

		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {

		final HttpServletResponse servletResponse = ((ServletServerHttpResponse) response).getServletResponse();

		if (servletResponse.getStatus() != 200) {

			return new String("error: {" + ((LinkedHashMap<Integer, String>) body).get("message") + "}");
		}

		return new Wrapper<Object>(body);
	}

	@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
	@JsonSerialize
	private class Wrapper<Object> {
		private final Object data;

		public Wrapper(Object data) {
			this.data = data;
		}
	}

}
