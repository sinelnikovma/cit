package ru.cip.contact.dao;

import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.cip.contact.model.Contact;

@Repository
public class ContactDaoImpl implements ContactDao {

	private final EntityManager em;

	@Autowired
	public ContactDaoImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public Set<Contact> getAll() {

		TypedQuery<Contact> query = em.createQuery("select c from Contact c", Contact.class);

		return query.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public Contact getById(Long id) {

		return em.find(Contact.class, id);
	}

	@Override
	public Set<Contact> getByName(String name) {

		if (name == null || name.isEmpty()) {
			return getAll();
		}

		CriteriaQuery<Contact> criteria = buildCriteria(name);

		TypedQuery<Contact> query = em.createQuery(criteria);

		return query.getResultStream().collect(Collectors.toSet());
	}

	private CriteriaQuery<Contact> buildCriteria(String name) {

		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Contact> criteria = builder.createQuery(Contact.class);

		Root<Contact> contact = criteria.from(Contact.class);
		criteria.where(builder.equal(contact.get("nameContact"), name));

		return criteria;
	}

}
