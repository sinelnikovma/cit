package ru.cip.contact.dao;

import java.util.Set;

import ru.cip.contact.model.Contact;

public interface ContactDao {

	public Set<Contact> getAll();

	public Contact getById(Long id);

	public Set<Contact> getByName(String name);
}
