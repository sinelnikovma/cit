package ru.cip.contact.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import ru.cip.phone.model.Phone;
import ru.cip.user.model.User;

@Entity
@Table(name = "contact")
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
	private Integer version;

	@Column(name = "type_contact")
	private String typeContact;

	@Column(name = "name_contact")
	private String nameContact;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "contact_phone", joinColumns = @JoinColumn(name = "contact_id"), inverseJoinColumns = @JoinColumn(name = "phone_id"))
	private Set<Phone> setPhone = new HashSet<>();

	public Contact() {
	}

	public Contact(String typeContact, String nameContact, User user) {
		this.typeContact = typeContact;
		this.nameContact = nameContact;
		this.user = user;
	}

	public Contact(String typeContact, String nameContact, User user, Set<Phone> setPhone) {
		this.typeContact = typeContact;
		this.nameContact = nameContact;
		this.user = user;
		this.setPhone = setPhone;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the typeContact
	 */
	public String getTypeContact() {
		return typeContact;
	}

	/**
	 * @param typeContact the typeContact to set
	 */
	public void setTypeContact(String typeContact) {
		this.typeContact = typeContact;
	}

	/**
	 * @return the nameContact
	 */
	public String getNameContact() {
		return nameContact;
	}

	/**
	 * @param nameContact the nameContact to set
	 */
	public void setNameContact(String nameContact) {
		this.nameContact = nameContact;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the setPhone
	 */
	public Set<Phone> getSetPhone() {
		return setPhone;
	}

	/**
	 * @param setPhone the setPhone to set
	 */
	public void setSetPhone(Set<Phone> setPhone) {
		this.setPhone = setPhone;
	}

}
