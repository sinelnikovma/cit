package ru.cip.phone.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import ru.cip.contact.model.Contact;

@Entity
@Table(name = "phone")
public class Phone {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
	private Integer version;

	@Column(name = "phone")
	private String phone;

	@ManyToMany(mappedBy = "setPhone")
	private Set<Contact> contact = new HashSet<>();

	public Phone() {
	}

	public Phone(String phone) {
		this.phone = phone;
	}

	public Phone(String phone, Set<Contact> contact) {
		this.phone = phone;
		this.contact = contact;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the contact
	 */
	public Set<Contact> getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(Set<Contact> contact) {
		this.contact = contact;
	}

}
