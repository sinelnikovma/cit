package ru.cip.phone.dao;

import java.util.Set;

import ru.cip.phone.model.Phone;

public interface PhoneDao {

	public Set<Phone> getAll();

	public Phone getById(Long id);

	public Set<Phone> getByName(String phone);
}
