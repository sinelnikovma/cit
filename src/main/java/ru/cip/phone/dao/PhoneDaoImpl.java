package ru.cip.phone.dao;

import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.cip.phone.model.Phone;

@Repository
public class PhoneDaoImpl implements PhoneDao {

	private EntityManager em;

	@Autowired
	public PhoneDaoImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public Set<Phone> getAll() {

		TypedQuery<Phone> query = em.createQuery("select p from Phone p", Phone.class);

		return query.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public Phone getById(Long id) {

		return em.find(Phone.class, id);
	}

	@Override
	public Set<Phone> getByName(String phone) {

		if (phone == null || phone.isEmpty()) {
			return getAll();
		}
		CriteriaQuery<Phone> criteria = buildCriteria(phone);

		TypedQuery<Phone> query = em.createQuery(criteria);

		return query.getResultStream().collect(Collectors.toSet());
	}

	private CriteriaQuery<Phone> buildCriteria(String phone) {

		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Phone> criteria = builder.createQuery(Phone.class);

		Root<Phone> phoneRoot = criteria.from(Phone.class);
		criteria.where(builder.equal(phoneRoot.get("phone"), phone));

		return criteria;
	}

}
