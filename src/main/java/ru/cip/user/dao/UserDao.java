package ru.cip.user.dao;

import java.util.Set;

import ru.cip.user.model.User;

public interface UserDao {

	public Set<User> getAll();

	public Set<User> getByFilter(User user);

	public User getById(Long id);
}
