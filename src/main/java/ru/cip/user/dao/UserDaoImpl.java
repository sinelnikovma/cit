package ru.cip.user.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.cip.user.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	private final EntityManager em;

	@Autowired
	public UserDaoImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public Set<User> getAll() {

		TypedQuery<User> query = em.createQuery("select u from User u", User.class);
		return query.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public Set<User> getByFilter(User user) {

		if (user == null || (user.getName().isEmpty() && user.getSurname().isEmpty())) {
			return getAll();
		}
		CriteriaQuery<User> criteriaQuery = builderQuery(user);

		TypedQuery<User> query = em.createQuery(criteriaQuery);

		return query.getResultStream().collect(Collectors.toSet());

	}

	private CriteriaQuery<User> builderQuery(User user) {

		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);
		Root<User> userRoot = criteriaQuery.from(User.class);
		criteriaQuery.select(userRoot);
		List<Predicate> predicateList = new ArrayList<>();

		if (user != null) {
			if (!user.getName().isEmpty()) {
				predicateList.add(cb.equal(userRoot.get("name"), user.getName()));
			}

			if (!user.getSurname().isEmpty()) {
				predicateList.add(cb.equal(userRoot.get("surname"), user.getSurname()));
			}
		}

		criteriaQuery.where(predicateList.toArray(new Predicate[] {}));

		return criteriaQuery;
	}

	@Override
	public User getById(Long id) {

		return em.find(User.class, id);
	}

}
