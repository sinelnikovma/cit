package ru.cip.user.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import ru.cip.contact.model.Contact;

@Entity
@Table(name = "usr")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
	private Integer version;

	@Column(name = "name")
	private String name;

	@Column(name = "sur_name")
	private String surname;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Contact> setContact = new HashSet<>();

	public User() {
	}

	public User(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public User(String name, String surname, Set<Contact> setContact) {
		this.name = name;
		this.surname = surname;
		this.setContact = setContact;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the setContact
	 */
	public Set<Contact> getSetContact() {
		return setContact;
	}

	/**
	 * @param setContact the setContact to set
	 */
	public void setSetContact(Set<Contact> setContact) {
		this.setContact = setContact;
	}

}
