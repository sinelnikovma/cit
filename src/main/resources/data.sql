INSERT INTO usr (id, version,name,sur_name) VALUES(1,0,'Вася','Пупкин');
INSERT INTO usr (id,version,name,sur_name) VALUES(2,0,'Петя','Федорчук');
INSERT INTO usr (id,version,name,sur_name) VALUES(3,0,'Коля','Пупкин');
INSERT INTO usr (id,version,name,sur_name) VALUES(4,0,'Ваня','Иванов');

INSERT INTO phone(id,version,phone) VALUES(1,0, '8(909)-555-11-99');
INSERT INTO phone(id,version,phone) VALUES(2,0, '8(909)-555-22-99');
INSERT INTO phone(id,version,phone) VALUES(3,0, '8(909)-555-33-99');
INSERT INTO phone(id,version,phone) VALUES(4,0, '8(909)-555-44-99');
INSERT INTO phone(id,version,phone) VALUES(5,0, '8(909)-555-55-99');
INSERT INTO phone(id,version,phone) VALUES(6,0, '8(909)-555-66-99');

INSERT INTO contact(id,version,type_contact,name_contact,user_id) VALUES(1,0,'VK','VKontakte',1);
INSERT INTO contact(id,version,type_contact,name_contact,user_id) VALUES(2,0,'FB','Facebook',2);
INSERT INTO contact(id,version,type_contact,name_contact,user_id) VALUES(3,0,'SK','Skype',1);
INSERT INTO contact(id,version,type_contact,name_contact,user_id) VALUES(4,0,'TG','Telegramm',3);
INSERT INTO contact(id,version,type_contact,name_contact,user_id) VALUES(5,0,'WA','WhatsApp',3);
INSERT INTO contact(id,version,type_contact,name_contact,user_id) VALUES(6,0,'VK','VKontakte',4);
INSERT INTO contact(id,version,type_contact,name_contact,user_id) VALUES(7,0,'SK','Skype',2);

INSERT INTO contact_phone (contact_id,phone_id) VALUES(1,2);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(1,4);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(2,3);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(4,1);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(5,2);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(6,3);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(6,1);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(3,2);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(3,4);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(5,1);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(1,6);
INSERT INTO contact_phone (contact_id,phone_id) VALUES(1,3);

