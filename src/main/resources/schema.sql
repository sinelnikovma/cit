 drop sequence IF exists hibernate_sequence;
 
 drop index if exists IX_contact_id;
 drop index if exists IX_contact_phone_id;
 drop index if exists IX_phone_contact_id;
 
 drop table if exists contact_phone;
 drop table if exists contact;
 drop table if exists phone;
 drop table if exists usr;
 
 create sequence IF NOT EXISTS hibernate_sequence start 1 increment 1;
 
 CREATE TABLE IF NOT EXISTS usr(
 id int8 not null,
 version int4 not null,
 name varchar(50),
 sur_name varchar(50) not null,
 primary key(id)
 );
 
 CREATE TABLE IF NOT EXISTS phone (
 id int8 not null,
 version int4 not null,
 phone varchar(20),
 primary key (id)
 );
 
 CREATE TABLE IF NOT EXISTS contact(
 id int8 not null,
 version int4 not null,
 type_contact varchar(100) not null, 
 name_contact varchar(100),
 user_id int8 not null,
 primary key(id)
 );
 
 CREATE TABLE IF NOT EXISTS contact_phone(
 contact_id int8 not null,
 phone_id int8 not null,
 primary key(contact_id,phone_id)
 );
 

 
 CREATE INDEX IX_contact_id ON contact(user_id);
 ALTER TABLE IF EXISTS  contact
 	ADD CONSTRAINT contact_user_fk
 	FOREIGN KEY (user_id) REFERENCES usr(id);
 
 CREATE INDEX IX_contact_phone_id ON contact_phone (phone_id);
 ALTER TABLE IF EXISTS  contact_phone
 	ADD CONSTRAINT contact_phone_fk
 	FOREIGN KEY (phone_id) REFERENCES phone(id);
 
 CREATE INDEX IX_phone_contact_id ON contact_phone (contact_id);
 ALTER TABLE IF EXISTS  contact_phone
 	ADD CONSTRAINT phone_contact_fk
 	FOREIGN KEY (contact_id) REFERENCES contact(id);